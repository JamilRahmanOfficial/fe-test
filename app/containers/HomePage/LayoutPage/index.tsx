import React from 'react';
import { Layout, theme } from 'antd';
import StyledCard from 'components/Card';
import Sider from '../Sider';
import Footer from 'components/Footer';

const { Header } = Layout;

const index = () => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    // <Layout>
    <Layout style={{ minHeight: '100vh' }}>
      <Sider />
      <Layout style={{ margin: '24px' }}>
        <Header style={{ padding: 0, background: colorBgContainer }} />
        Bill is a cat.
      </Layout>
    </Layout>
  );
};

export default index;
