import React from 'react';

import Wrapper from './Wrapper';

function Footer() {
  return <Wrapper>This is the footer section</Wrapper>;
}

export default Footer;
