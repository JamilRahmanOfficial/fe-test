import styled from 'styles/styled-components';
import { Card } from 'antd';

const StyledCard = styled(Card)`
  border-radius: 7px;
  margin: 24px;
`;

export default StyledCard;
